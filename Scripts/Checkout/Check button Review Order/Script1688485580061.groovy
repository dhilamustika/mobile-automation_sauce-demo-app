import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Checkout/Check button To Payment'), [:], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Checkout Product/inputCardFullName'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.sendKeys(findTestObject('Object Repository/Checkout Product/inputCardFullName'), 'Patrick Star', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.hideKeyboard(FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Checkout Product/inputCardNumber'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.sendKeys(findTestObject('Object Repository/Checkout Product/inputCardNumber'), '123456789012345', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.hideKeyboard(FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Checkout Product/inputExpirationDate'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.sendKeys(findTestObject('Object Repository/Checkout Product/inputExpirationDate'), '11/25', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Checkout Product/inputSecurityCode'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.sendKeys(findTestObject('Object Repository/Checkout Product/inputSecurityCode'), '125', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.hideKeyboard(FailureHandling.CONTINUE_ON_FAILURE)

Mobile.scrollToText('Review Order', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Checkout Product/buttonReviewOrder'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Checkout Product/buttonReviewOrder'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Checkout Product/titleReviewYourOrder'), 0, FailureHandling.STOP_ON_FAILURE)

